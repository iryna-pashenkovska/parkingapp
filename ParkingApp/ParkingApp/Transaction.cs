﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp
{
    public class Transaction
    {
        public DateTime transactionTime { get; set; }
        public string vehicleId { get; set; }
        public double paymentAmount { get; set; }

        public Transaction(DateTime transTime, string vId, double amount)
        {
            transactionTime = transTime;
            vehicleId = vId;
            paymentAmount = amount;
        }

        public string TransactionToLogFile()
        {
            return transactionTime.ToString() + "," + vehicleId + "," + paymentAmount.ToString();
        }
    }
}
