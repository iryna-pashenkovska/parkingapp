﻿using ParkingApp.TransportTypes;
using System.Timers;
using ParkingApp.Helpers;
using System;

namespace ParkingApp
{
    public class Vehicle
    {
        private static int idGenerator = 0;

        public VehicleType vehicleType { get; set; }
        public double balance { get; set; }
        public Timer timer { get; set; }

        public string Id { get; }

        public Vehicle(VehicleType vt, double bal)
        {
            vehicleType = vt;
            balance = bal;
            timer = new Timer(ParkingAppSettings.paymentTimeIntervalInSeconds * 1000);

            idGenerator++;
            Id = vehicleType.ToString() + "_" + idGenerator.ToString();
        }

        public override string ToString()
        {
            return Id + " " + vehicleType.ToString() + " " + balance.ToString();
        }
    }
}
