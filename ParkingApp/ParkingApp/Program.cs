﻿using System;
using System.IO;
using ParkingApp.Helpers;

namespace ParkingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuHandler.parking = Parking.ParkingInstance;
            MenuHandler.DrawMenu();
            bool cont = true;
            do
            {
                try
                {
                    Console.WriteLine();
                    Console.Write("Your choise: ");
                    var action = Console.ReadLine();
                    Console.Clear();
                    MenuHandler.DrawMenu();
                    cont = MenuHandler.SelectMenuOption(action);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (cont);

            if (File.Exists(Parking.logFilePath))
            {
                File.Delete(Parking.logFilePath);
            }
        }
    }
}
