﻿using ParkingApp.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;

namespace ParkingApp
{
    public class Parking
    {
        private static Parking parkingInstance;
        public double balance;
        public List<Vehicle> parkedVehicles;
        private List<Transaction> transactionLogLastMinute;
        public static string logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Transaction.log");

        private Parking()
        {
            balance = ParkingAppSettings.initialParkingBalance;
            transactionLogLastMinute = new List<Transaction>();
            parkedVehicles = new List<Vehicle>();

            Timer timer = new Timer(60000);
            timer.Elapsed += WriteTransactionsToLogFile;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void NormalizeLastMinuteTransactionLog()
        {
            transactionLogLastMinute = transactionLogLastMinute.Where(t => t.transactionTime > DateTime.Now.AddMinutes(-1)).ToList();
        }

        private void WriteTransactionsToLogFile(object sender, ElapsedEventArgs e)
        {
            if (transactionLogLastMinute != null)
            {
                try
                {
                    using (StreamWriter outputFile = new StreamWriter(logFilePath, true))
                    {
                        foreach (var transaction in transactionLogLastMinute)
                            outputFile.WriteLine(transaction.TransactionToLogFile());
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    throw;
                }
            }
        }

        public static void ReadTransactionsFromLogFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(logFilePath))
                {
                    string line = sr.ReadToEnd();
                    Console.WriteLine(line);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Problem with reading from file");
                Console.WriteLine(e.Message);
            }
        }

        public static Parking ParkingInstance
        {
            get
            {
                if (parkingInstance == null)
                {
                    parkingInstance = new Parking();
                }

                return parkingInstance;
            }
        }

        public List<Transaction> TransactionLogLastMinute
        {
            get
            {
                return transactionLogLastMinute = transactionLogLastMinute.Where(t => t.transactionTime > DateTime.Now.AddMinutes(-1)).ToList();
            }
        }

        public void AddVehicle(Vehicle v)
        {          
            v.timer.Elapsed += (sender, e) => CreateTransaction(sender, e, v); ;
            v.timer.AutoReset = true;
            v.timer.Enabled = true;
            v.timer.Start();

            parkedVehicles.Add(v);
        }

        public void RemoveVehicle(Vehicle v)
        {
            v.timer.Stop();
            parkedVehicles.Remove(v);
        }

        public void AddTransaction(Transaction t)
        {
            transactionLogLastMinute.Add(t);
        }

        public void CreateTransaction(object sender, ElapsedEventArgs e, Vehicle v)
        {
            double amount = 0;
            if (v.balance > ParkingAppSettings.rates[v.vehicleType])
            {
                amount = ParkingAppSettings.rates[v.vehicleType];
            }
            else
            {
                amount = ParkingAppSettings.rates[v.vehicleType] * ParkingAppSettings.penaltyRatio;
            }
            var transaction = new Transaction(DateTime.Now, v.Id, amount);
            AddTransaction(transaction);
            balance += amount;
            v.balance -= amount;
        }
    }
}
