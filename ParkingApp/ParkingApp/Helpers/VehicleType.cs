﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.TransportTypes
{
    public enum VehicleType
    {
        bus = 1,
        car = 2,
        motorcycle = 3,
        truck = 4
    };
}
