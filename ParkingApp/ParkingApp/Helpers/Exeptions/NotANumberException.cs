﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Helpers.Exeptions
{
    class NotANumberException :FormatException
    {
        public NotANumberException()
        {

        }

        public NotANumberException(string number)
            : base(String.Format("Wrong data format. Input should be a number: {0}", number))
        {

        }
    }
}
