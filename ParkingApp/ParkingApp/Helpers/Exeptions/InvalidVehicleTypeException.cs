﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Helpers.Exeptions
{
    class InvalidVehicleTypeException: ArgumentException
    {
        public InvalidVehicleTypeException()
        {

        }
 
        public InvalidVehicleTypeException(string vehicleType)
            : base(String.Format("Invalid Vehicle Type: {0}", vehicleType))
        {

        }
    }
}
