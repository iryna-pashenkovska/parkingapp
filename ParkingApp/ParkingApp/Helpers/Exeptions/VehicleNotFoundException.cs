﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Helpers.Exeptions
{
    class VehicleNotFoundException: Exception
    {
        public VehicleNotFoundException()
        {

        }

        public VehicleNotFoundException(string vehicleId)
            : base(String.Format("Vehicle with ID '{0}' doesn't exist.", vehicleId))
        {

        }
    }
}
