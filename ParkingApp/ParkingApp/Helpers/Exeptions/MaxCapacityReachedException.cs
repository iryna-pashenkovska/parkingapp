﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingApp.Helpers.Exeptions
{
    class MaxCapacityReachedException: Exception
    {
        public MaxCapacityReachedException() :
            base(String.Format("No free places on the Parking."))
        {

        }
    }
}
