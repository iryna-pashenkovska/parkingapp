﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingApp.TransportTypes;

namespace ParkingApp.Helpers
{
    public static class ParkingAppSettings
    {
        public static double initialParkingBalance = 0;
        public static int maxParkingCapacity = 10;
        public static int paymentTimeIntervalInSeconds = 5;
        public static double penaltyRatio = 2.5;
        public static Dictionary<VehicleType, double> rates = new Dictionary<VehicleType, double>()
        {
            {VehicleType.car, 2},
            {VehicleType.truck, 5},
            {VehicleType.bus, 3.5},
            {VehicleType.motorcycle, 1}
        };
    }
}
