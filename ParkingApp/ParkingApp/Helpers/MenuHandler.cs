﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParkingApp.Helpers.Exeptions;
using ParkingApp.TransportTypes;

namespace ParkingApp.Helpers
{
    public static class MenuHandler
    {
        public static Parking parking;
        private static void DrawStarLine()
        {
            Console.WriteLine("***********************");
        }
        private static void DrawTitle()
        {
            DrawStarLine();
            Console.WriteLine("+++   PARKING APP   +++");
            DrawStarLine();
        }
        public static void DrawMenu()
        {
            DrawTitle();
            DrawStarLine();
            Console.WriteLine(" 1. Parking balance");
            Console.WriteLine(" 2. Money earned in last minute");
            Console.WriteLine(" 3. Show number of free/occuppied places");
            Console.WriteLine(" 4. Show all transactions made in last minute");
            Console.WriteLine(" 5. Show all transactions");
            Console.WriteLine(" 6. Show list of all vehicles");
            Console.WriteLine(" 7. Put vehicle on the parking place");
            Console.WriteLine(" 8. Pick up vehicle from parking place");
            Console.WriteLine(" 9. Recharge the balance of the vehicle");
            DrawStarLine();
            Console.WriteLine("Make your choice: type 1, 2,... or {0} for exit", 0);
            DrawStarLine();
        }

        public static bool SelectMenuOption(string action)
        {
            bool cont = true;
            switch (action.Trim())
            {
                case "1":
                    ShowParkingBalance();
                    break;
                case "2":
                    EarnedLastMinute();
                    break;
                case "3":
                    ShowParkingPlacesStatus();
                    break;
                case "4":
                    ShowLastMinuteTransactions();
                    break;
                case "5":
                    ShowAllTransactions();
                    break;
                case "6":
                    ShowVehicles();
                    break;
                case "7":
                    PutVehicleOnParking();
                    break;
                case "8":
                    PickUpVehicleFromParking();
                    break;
                case "9":
                    RechargeVehicleBalance();
                    break;
                case "0":
                    cont = false;
                    break;
            }

            return cont;
        }

        private static void ShowParkingBalance()
        {
            Console.WriteLine("Parking balance = {0}", parking.balance);
        }

        private static void EarnedLastMinute()
        {
            parking.NormalizeLastMinuteTransactionLog();

            var lastMinuteProfit = parking.TransactionLogLastMinute.Select(t => t.paymentAmount).Sum();
            Console.WriteLine("Earned during last minute = {0}", lastMinuteProfit);
        }

        private static void ShowParkingPlacesStatus()
        {
            var occuppiedPlaces = parking.parkedVehicles.Count;
            var freePlaces = ParkingAppSettings.maxParkingCapacity - occuppiedPlaces;
            Console.WriteLine("Free - {0}, Occuppied - {1}", freePlaces, occuppiedPlaces);
        }

        private static void ShowLastMinuteTransactions()
        {
            if (parking.TransactionLogLastMinute.Count == 0)
            {
                Console.WriteLine("No transaction done in last minute.");
            }
            else
            {
                parking.NormalizeLastMinuteTransactionLog();

                foreach (var transaction in parking.TransactionLogLastMinute)
                {
                    Console.WriteLine(transaction.TransactionToLogFile());
                }
            }
        }

        private static void ShowAllTransactions()
        {
            Parking.ReadTransactionsFromLogFile();
        }

        private static void ShowVehicles()
        {
            if (parking.parkedVehicles.Count == 0)
            {
                Console.WriteLine("Parking is empty.");
            }
            else
            {
                foreach (var vehicle in parking.parkedVehicles)
                {
                    Console.WriteLine(vehicle.ToString());
                }
            }
        }

        private static void PutVehicleOnParking()
        {
            if(parking.parkedVehicles.Count == ParkingAppSettings.maxParkingCapacity)
                throw new MaxCapacityReachedException();

            string vehicleType = "";
            string vehicleBalance = "";

            try
            {
                vehicleType = AskUser("vehicle type").ToLower();
                var vt = (VehicleType)Enum.Parse(typeof(VehicleType), vehicleType);
                vehicleBalance = AskUser("vehicle balance");
                var vb = Convert.ToDouble(vehicleBalance);
                var newVehicle = new Vehicle(vt, vb);

                parking.AddVehicle(newVehicle);
            }
            catch (FormatException)
            {
                throw new NotANumberException(vehicleBalance);
            }
            catch (ArgumentException)
            {
                throw new InvalidVehicleTypeException(vehicleType);
            }
        }

        private static Vehicle GetVehicleById(string vehicleId)
        {
            var vehicleToRecharge = parking.parkedVehicles.Find(v => v.Id == vehicleId);

            if (vehicleToRecharge == null)
                throw new VehicleNotFoundException(vehicleId);
            
            return vehicleToRecharge;
        }

        private static void PickUpVehicleFromParking()
        {
            var vehicleId = AskUser("vehicle ID").ToLower();
            var vehicleToRemove = GetVehicleById(vehicleId);
            parking.RemoveVehicle(vehicleToRemove);
        }

        private static void RechargeVehicleBalance()
        {
            string vehicleRechargeAmount = "";

            try
            {
                var vehicleId = AskUser("vehicle ID").ToLower();
                var vehicleToRecharge = GetVehicleById(vehicleId);
                vehicleRechargeAmount = AskUser(string.Format("amount for recharging vehicke {0}", vehicleId));
                var ra = Convert.ToDouble(vehicleRechargeAmount);

                vehicleToRecharge.balance += ra;
            }
            catch (FormatException)
            {
                throw new NotANumberException(vehicleRechargeAmount);
            }

        }

        private static string AskUser(string info)
        {
            Console.WriteLine("Please enter {0}: ", info);
            return Console.ReadLine();
        }
    }
}
